import unittest
from HelloPython import calc

class TestCalc(unittest.TestCase):
    def test_addData(self):
        actual = calc.addData(1, 2)
        self.assertEqual(actual, 3)

if __name__ == '__main__':
    unittest.main()